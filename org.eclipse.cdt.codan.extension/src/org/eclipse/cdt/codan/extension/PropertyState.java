/*******************************************************************************
 * Copyright (c) 2009 Elliott Baron
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Elliott Baron - initial API and implementation
 *******************************************************************************/
package org.eclipse.cdt.codan.extension;

import org.eclipse.cdt.core.dom.ast.IASTNode;

/**
 * A Property State represents the state of a temporal property.
 * One or more Property States comprise an {@link IPropertyFSM}.
 * Statements and expressions in the AST can cause a transition between
 * states.
 */
public abstract class PropertyState {	
	private String name;
	
	/**
	 * Constructs an anonymous Property State.
	 */
	public PropertyState() {
		this(null);
	}
	
	/**
	 * Constructs a Property State with an optional name used
	 * to label it.
	 * @param name - some meaningful name
	 */
	public PropertyState(String name) {
		this.name = name;
	}
	
	/**
	 * Called while processing a control flow. Determine if node's
	 * contents cause this state to transition to another state according
	 * to the rules of your temporal property's finite state machine.
	 * @param node - the IASTNode currently being processed
	 * @return the PropertyState we transition to, or this if no transition
	 */
	public abstract PropertyState transition(IASTNode node);
	
	@Override
	public String toString() {
		String ret;
		if (name != null) {
			ret = name;
		}
		else {
			ret = super.toString();
		}
		return ret;
	}
	
}
