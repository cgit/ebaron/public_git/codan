/*******************************************************************************
 * Copyright (c) 2009 Elliott Baron
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Elliott Baron - initial API and implementation
 *******************************************************************************/
package org.eclipse.cdt.codan.extension;

import java.util.Set;

/**
 * A finite state machine that encodes a temporal property.
 * It is a set of Property States with defined meanings and transitions.
 */
public interface IPropertyFSM {
	
	/**
	 * Returns all Property States in this finite state machine.
	 * @return a Set of all Property States
	 */
	public Set<PropertyState> getPropertyStates();
	
	/**
	 * @return the initial state for this finite state machine
	 */
	public PropertyState getUninitState();
	
	/**
	 * @return the error state indicating the temporal property was violated
	 */
	public PropertyState getErrorState();

}
