/*******************************************************************************
 * Copyright (c) 2009 Elliott Baron
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Elliott Baron - initial API and implementation
 *******************************************************************************/
package org.eclipse.cdt.codan.extension;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.cdt.codan.extension.Minterm.Value;
import org.eclipse.cdt.core.dom.ast.IVariable;

public class ESSimplifier extends QuineMcCluskeySimplifier<IVariable> {
	private Map<ExecutionState, Minterm<IVariable>> esMapping;

	public ESSimplifier(ESTruthTable tt) {
		super(tt);
		esMapping = new HashMap<ExecutionState, Minterm<IVariable>>();
	}

	public Set<ExecutionState> getExecutionStateCover() {
		Set<ExecutionState> ret = new HashSet<ExecutionState>();
		List<Minterm<IVariable>> implicantCover = getImplicantCover();
		for (Minterm<IVariable> term : implicantCover) {
			ExecutionState es = new ExecutionState();
			for (IVariable var : truthTable.getVariables()) {
				Value val = term.getValue(var);
				if (val == Value.TRUE) {
					es.addClause(new ExecutionStateClause(var, true));
				}
				else if (val == Value.FALSE) {
					es.addClause(new ExecutionStateClause(var, false));
				}
				// Don't care about Don't cares
			}
			// Add truth assignments from implied minterms
			List<Minterm<IVariable>> implied = getImplications(term);
			for (Minterm<IVariable> minterm : implied) {
				ExecutionState e = ((ESTruthTable) truthTable).getExecutionState(minterm);
				es.getTruthAssignments().putAll(e.getTruthAssignments());
			}
			ret.add(es);
			esMapping.put(es, term);
		}
		return ret;
	}
	
	public Set<ExecutionState> getImplications(ExecutionState primeImplicant) {
		Set<ExecutionState> ret = new HashSet<ExecutionState>();
		Minterm<IVariable> piTerm = esMapping.get(primeImplicant);
		List<Minterm<IVariable>> impls = getImplications(piTerm);
		for (Minterm<IVariable> term : impls) {
			ExecutionState es = ((ESTruthTable) truthTable).getExecutionState(term);
			ret.add(es);
		}
		return ret;
	}
	
	@Override
	public String toString() {
		StringBuffer buf = new StringBuffer();
		for (IVariable var : truthTable.getVariables()) {
			buf.append(var.getName());
			buf.append(" ");
		}
		buf.append("\n");
		for (Minterm<IVariable> term : truthTable.getMinterms()) {
			for (IVariable var : truthTable.getVariables()) {
				switch (term.getValue(var)) {
				case TRUE:
					buf.append("1");
					break;
				case FALSE:
					buf.append("0");
					break;
				default:
					buf.append("-");
				}
				buf.append(" ");
			}
			buf.append("\n");
		}
		return buf.toString();
	}
}
