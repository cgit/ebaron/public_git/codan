/*******************************************************************************
 * Copyright (c) 2009 Elliott Baron
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Elliott Baron - initial API and implementation
 *******************************************************************************/
package org.eclipse.cdt.codan.extension;

import java.util.HashMap;
import java.util.Map;

public class Minterm<E> {
	private Map<E, Value> values;

	public enum Value {
		TRUE,
		FALSE,
		DONTCARE
	}

	public Minterm(Map<E, Value> values) {
		this.values = values;
	}

	public void setValue(E var, Value val) {
		values.put(var, val);
	}

	public Value getValue(E var) {
		return values.get(var);
	}

	public int length() {
		return values.size();
	}

	public Minterm<E> copy() {
		return new Minterm<E>(new HashMap<E, Value>(values));
	}

	public Minterm<E> combineTerms(Minterm<E> other) {
		Minterm<E> ret = null;
		E diff = null;
		for (E var : values.keySet()) {
			if (!values.get(var).equals(other.getValue(var))) {
				if (diff != null) {
					// More than one difference
					diff = null;
					break;
				}
				diff = var;
			}
		}
		
		// One difference, combine
		if (diff != null) {
			ret = copy();
			// Set the differing value to don't care
			ret.setValue(diff, Value.DONTCARE);
		}
		
		return ret;
	}
	
	public boolean implies(Minterm<E> other) {
		boolean result = true;
		for (E var : values.keySet()) {
			Value value = values.get(var);
			if (value != Value.DONTCARE && value != other.getValue(var)) {
				result = false;
			}
		}
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Minterm<?>) {
			Minterm<?> other = (Minterm<?>) obj;
			return values.equals(other.values);
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return values.hashCode();
	}
	
	@Override
	public String toString() {
		StringBuffer buf = new StringBuffer();
		for (E k : values.keySet()) {
			buf.append(k);
			buf.append(" ");
		}
		buf.append("\n");
		for (Value v : values.values()) {
			switch (v) {
			case TRUE:
				buf.append("1");
				break;
			case FALSE:
				buf.append("0");
				break;
			default:
				buf.append("-");
			}
			buf.append(" ");
		}
		return buf.toString();	
	}
}
