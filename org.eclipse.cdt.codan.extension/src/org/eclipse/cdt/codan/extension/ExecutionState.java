/*******************************************************************************
 * Copyright (c) 2009 Elliott Baron
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Elliott Baron - initial API and implementation
 *******************************************************************************/
package org.eclipse.cdt.codan.extension;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.cdt.core.dom.ast.IVariable;
import org.eclipse.ptp.pldt.mpi.analysis.cdt.graphs.IBlock;

public class ExecutionState {
	private Set<ExecutionStateClause> clauses;
	private Map<IVariable, TruthAssignment> truthAssignments; // FIXME this should be more robust, use SSA
	private boolean top;
	private boolean bottom;

	public ExecutionState() {
		truthAssignments = new HashMap<IVariable, TruthAssignment>();
		clauses = new HashSet<ExecutionStateClause>();
		top = true;
		bottom = false;
	}

	public void addClause(ExecutionStateClause clause) {
		// Cannot get out of bottom, do not add duplicate clauses
		if (!bottom && !clauses.contains(clause)) {
			top = false; // T AND x = x
			if (clauses.contains(clause.getNegation())) {
				clauses.clear();
				bottom = true; // x AND NOT x = F
			} else {
				clauses.add(clause);
			}
		}
	}

	public void removeClause(ExecutionStateClause node) {
		clauses.remove(node);
		if (clauses.size() == 0) {
			setTop();
		}
	}

	public void addTruthAssignment(IVariable var, boolean value, IBlock blk) {
		truthAssignments.put(var, new TruthAssignment(blk, value));
	}

	public Map<IVariable, TruthAssignment> getTruthAssignments() {
		return truthAssignments;
	}

	public void setTruthAssignments(Map<IVariable, TruthAssignment> truthAssignments) {
		this.truthAssignments = truthAssignments;
	}

	public void bindTruthAssignments() {
		boolean unsatisfiable = false;
		Set<ExecutionStateClause> clausesCopy = new HashSet<ExecutionStateClause>(
				clauses);
		for (ExecutionStateClause c : clausesCopy) {
			TruthAssignment ta = truthAssignments.get(c.getVariable());
			if (ta != null) {
				Boolean truth = ta.getValue();
				if (truth != null) {
					if (truth == c.isTrue()) { // does truth value of clause match
						// truth assignment?
						removeClause(c);
					} else {
						unsatisfiable = true;
					}
				}
			}
		}

		if (unsatisfiable) {
			setBottom();
		}
	}

	public Set<ExecutionStateClause> getClauses() {
		return clauses;
	}

	public boolean isTop() {
		return top;
	}

	public boolean isBottom() {
		return bottom;
	}

	public void setTop() {
		top = true;
		bottom = false;
		clauses.clear();
	}

	public void setBottom() {
		bottom = true;
		top = false;
		clauses.clear();
	}

	@Override
	public String toString() {
		String ret;
		if (top) {
			ret = "[TOP]";
		} else if (bottom) {
			ret = "[BOTTOM]";
		} else {
			StringBuffer buf = new StringBuffer();
			if (clauses.size() > 0) {
				for (ExecutionStateClause c : clauses) {
					buf.append(c);
					buf.append(" AND ");
				}
				buf.delete(buf.length() - 5 /* " AND ".length() */, buf
						.length());
			}
			ret = buf.toString();
		}
		return ret;
	}

	public ExecutionState copy() {
		ExecutionState ret = new ExecutionState();
		for (ExecutionStateClause cl : clauses) {
			ret.addClause(cl);
		}
		return ret;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof ExecutionState) {
			ExecutionState other = (ExecutionState) obj;
			return clauses.equals(other.getClauses())
					&& truthAssignments.equals(other.getTruthAssignments())
					&& top == other.isTop() && bottom == other.isBottom();
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return clauses.hashCode() + truthAssignments.hashCode() + (top ? 1 : 0) + (bottom ? 1 : 0);
	}

}
