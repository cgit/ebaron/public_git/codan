/*******************************************************************************
 * Copyright (c) 2009 Elliott Baron
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Elliott Baron - initial API and implementation
 *******************************************************************************/
package org.eclipse.cdt.codan.extension;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.cdt.core.dom.ast.IASTNode;
import org.eclipse.cdt.core.dom.ast.IVariable;

public class SymbolicState {
	private Set<PropertyState> propertyStates;
	private ExecutionState executionState;
	private Set<IASTNode> errorCauses;
	
	public SymbolicState(Set<PropertyState> propertyStates, ExecutionState executionState) {
		this.propertyStates = propertyStates;
		this.executionState = executionState;
		errorCauses = new HashSet<IASTNode>();
	}

	public ExecutionState getExecutionState() {
		return executionState;
	}
	
	public void setExecutionState(ExecutionState es) {
		executionState = es;
	}
	
	public Set<PropertyState> getPropertyStates() {
		return Collections.unmodifiableSet(propertyStates);
	}
	
	public void setPropertyStates(Set<PropertyState> ps) {
		propertyStates = ps;
	}
	
	public SymbolicState copy() {
		Set<PropertyState> ps = new HashSet<PropertyState>(propertyStates);
		ExecutionState es = executionState.copy();
		Map<IVariable, TruthAssignment> truthAssignments = new HashMap<IVariable, TruthAssignment>(executionState.getTruthAssignments());
		es.setTruthAssignments(truthAssignments);
		
		SymbolicState ret = new SymbolicState(ps, es);
		ret.errorCauses = new HashSet<IASTNode>(errorCauses);
		return ret;
	}
	
	public Set<IASTNode> getErrorCauses() {
		return errorCauses;
	}
	
	@Override
	public String toString() {
		StringBuffer buf = new StringBuffer();
		buf.append("[");
		buf.append(propertyStates);
		buf.append(", ");
		buf.append(executionState);
		buf.append("]");
		return buf.toString();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof SymbolicState) {
			SymbolicState other = (SymbolicState) obj;
			return propertyStates.equals(other.getPropertyStates()) && executionState.equals(other.getExecutionState());
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return propertyStates.hashCode() + executionState.hashCode();
	}
}
