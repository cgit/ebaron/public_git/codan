/*******************************************************************************
 * Copyright (c) 2009 Elliott Baron
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Elliott Baron - initial API and implementation
 *******************************************************************************/
package org.eclipse.cdt.codan.extension;

import org.eclipse.ptp.pldt.mpi.analysis.cdt.graphs.IBlock;

public class TruthAssignment {	
	private IBlock blk;
	private Boolean value;

	public TruthAssignment(IBlock blk, Boolean value) {
		this.blk = blk;
		this.value = value;
	}
	
	public IBlock getOrigin() {
		return blk;
	}
	
	public Boolean getValue() {
		return value;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof TruthAssignment) {
			TruthAssignment other = (TruthAssignment) obj;
			return blk.equals(other.getOrigin()) && value.equals(other.getValue());
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return blk.hashCode() + value.hashCode();
	}

}
