/*******************************************************************************
 * Copyright (c) 2009 Elliott Baron
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Elliott Baron - initial API and implementation
 *******************************************************************************/
package org.eclipse.cdt.codan.extension;

import org.eclipse.cdt.core.dom.ast.ASTTypeUtil;
import org.eclipse.cdt.core.dom.ast.IASTCastExpression;
import org.eclipse.cdt.core.dom.ast.IASTExpression;
import org.eclipse.cdt.core.dom.ast.IASTLiteralExpression;
import org.eclipse.cdt.core.dom.ast.IASTTypeId;
import org.eclipse.cdt.core.dom.ast.IASTUnaryExpression;

public class ASTParserUtil {

	public static Boolean getTruthValue(IASTExpression valueExpr) {
		Boolean result = null;
		// Handle assignment from literals
		result = getLiteralTruth(valueExpr);
		// Handle NULL assignment, assume NULL is defined as (void *)0
		if (valueExpr instanceof IASTUnaryExpression) {
			IASTExpression op = ((IASTUnaryExpression) valueExpr).getOperand();
			if (op instanceof IASTCastExpression) {
				IASTCastExpression castExpr = (IASTCastExpression) op;
				IASTTypeId castType = castExpr.getTypeId();
				String typeString = ASTTypeUtil.getType(castType);
				if (typeString.equals("void *")) {
					result = getLiteralTruth(castExpr.getOperand());
				}				
			}
		}
		// TODO other variable assignments
		return result;
	}

	private static Boolean getLiteralTruth(IASTExpression valueExpr) {
		Boolean result = null;
		if (valueExpr instanceof IASTLiteralExpression) {
			int kind = ((IASTLiteralExpression) valueExpr).getKind();
			String value = String.valueOf(((IASTLiteralExpression) valueExpr).getValue());
			switch (kind) {
			case IASTLiteralExpression.lk_integer_constant:
				// 0 = False, > 0 = True
				result = !value.equals("0");
			}
		}
		return result;
	}
	
}
