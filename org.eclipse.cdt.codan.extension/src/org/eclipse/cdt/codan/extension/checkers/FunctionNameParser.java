/*******************************************************************************
 * Copyright (c) 2009 Elliott Baron
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Elliott Baron - initial API and implementation
 *******************************************************************************/
package org.eclipse.cdt.codan.extension.checkers;

import org.eclipse.cdt.core.dom.ast.ASTTypeUtil;
import org.eclipse.cdt.core.dom.ast.ASTVisitor;
import org.eclipse.cdt.core.dom.ast.DOMException;
import org.eclipse.cdt.core.dom.ast.IASTExpression;
import org.eclipse.cdt.core.dom.ast.IASTFunctionCallExpression;
import org.eclipse.cdt.core.dom.ast.IASTIdExpression;
import org.eclipse.cdt.core.dom.ast.IASTName;
import org.eclipse.cdt.core.dom.ast.IASTNode;
import org.eclipse.cdt.core.dom.ast.IBinding;
import org.eclipse.cdt.core.dom.ast.IFunction;
import org.eclipse.cdt.core.dom.ast.IParameter;
import org.eclipse.cdt.core.dom.ast.IType;

public class FunctionNameParser {
	private IASTNode node;
	private String funcName;
	private String[] paramTypes;
	
	public FunctionNameParser(IASTNode node, String funcName, String[] paramTypes) {
		this.node = node;
		this.funcName = funcName;
		this.paramTypes = paramTypes;
	}
	
	public boolean matches() {
		FunctionNameVisitor visitor = new FunctionNameVisitor();
		node.accept(visitor);
		return visitor.getResult();
	}

	private class FunctionNameVisitor extends ASTVisitor {
		private boolean result;
		
		public FunctionNameVisitor() {
			shouldVisitExpressions = true;
		}
		
		@Override
		public int visit(IASTExpression expr) {
			if (expr instanceof IASTFunctionCallExpression) {
				IASTFunctionCallExpression callExpression = (IASTFunctionCallExpression) expr;
				IASTExpression funcExpr = callExpression.getFunctionNameExpression();
				if (funcExpr instanceof IASTIdExpression) {
					IASTName name = ((IASTIdExpression) funcExpr).getName();
					IBinding binding = name.resolveBinding();
					if (binding instanceof IFunction) {
						IFunction fun = (IFunction) binding;
						String funName = fun.getName();
						if (funcName.equals(funName)) {
							try {
								IParameter[] params = fun.getParameters();
								if (paramTypes.length == params.length && paramTypes.length > 0) {
									result = true;
									for (int i = 0; i < params.length; i++) {
										IType type = params[i].getType();
										String actualType = ASTTypeUtil.getType(type, false);
										result &= paramTypes[i].equals(actualType);
									}
								}
							} catch (DOMException e) {
								e.printStackTrace();
							}
						}
					}
					return PROCESS_SKIP;
				}
			}
			return PROCESS_CONTINUE;
		}
		
		public boolean getResult() {
			return result;
		}
		
	}
}
