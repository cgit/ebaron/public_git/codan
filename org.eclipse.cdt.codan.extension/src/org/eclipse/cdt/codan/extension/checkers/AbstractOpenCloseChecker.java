/*******************************************************************************
 * Copyright (c) 2009 Elliott Baron
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Elliott Baron - initial API and implementation
 *******************************************************************************/
package org.eclipse.cdt.codan.extension.checkers;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.cdt.codan.extension.ExecutionState;
import org.eclipse.cdt.codan.extension.IPropertyFSM;
import org.eclipse.cdt.codan.extension.PropertyState;
import org.eclipse.cdt.core.dom.ast.IASTNode;

public abstract class AbstractOpenCloseChecker extends AbstractPropSimChecker {	
	private IPropertyFSM fsm;
	
	// Property FSM states
	private PropertyState uninit;
	private PropertyState error;
	private PropertyState opened;

	public AbstractOpenCloseChecker() {
		fsm = initFSM();
	}	
	
	private IPropertyFSM initFSM() {
		uninit = new PropertyState("$u") {			
			@Override
			public PropertyState transition(IASTNode node) {
				PropertyState dest = uninit;
				if (containsOpen(node)) {
					dest = opened;
				}
				else if (containsClose(node)) {
					dest = error;
				}
				return dest;
			}
		};
		
		opened = new PropertyState("o") {			
			@Override
			public PropertyState transition(IASTNode node) {
				PropertyState dest = opened;
				if (containsOpen(node)) {
					dest = error;
				}
				if (containsClose(node)) {
					dest = uninit;
				}
				return dest;
			}
		};
		
		error = new PropertyState("$e") {
			
			@Override
			public PropertyState transition(IASTNode node) {
				return error;
			}
		};
		
		return new IPropertyFSM() {
			
			@Override
			public PropertyState getUninitState() {
				return uninit;
			}
			
			@Override
			public Set<PropertyState> getPropertyStates() {
				Set<PropertyState> states = new HashSet<PropertyState>();
				states.add(uninit);
				states.add(opened);
				states.add(error);
				return states;
			}
			
			@Override
			public PropertyState getErrorState() {
				return error;
			}
		};
	}
	
	@Override
	protected IPropertyFSM getPropertyFSM() {
		return fsm;
	}
	
	protected abstract boolean containsOpen(IASTNode node);

	protected abstract boolean containsClose(IASTNode node);
	
	protected abstract void reportProblem(IASTNode node, ExecutionState condition);
}
