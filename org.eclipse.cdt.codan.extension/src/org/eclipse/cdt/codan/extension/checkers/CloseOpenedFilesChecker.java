/*******************************************************************************
 * Copyright (c) 2009 Elliott Baron
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Elliott Baron - initial API and implementation
 *******************************************************************************/
package org.eclipse.cdt.codan.extension.checkers;

import java.text.MessageFormat;

import org.eclipse.cdt.codan.extension.Activator;
import org.eclipse.cdt.codan.extension.ExecutionState;
import org.eclipse.cdt.core.dom.ast.IASTNode;

public class CloseOpenedFilesChecker extends AbstractOpenCloseChecker {
	private static final String ERR_ID = Activator.PLUGIN_ID + ".OpenCloseFilesProblem";
	
	private static final String OPEN = "open";
	private static final String CLOSE = "close";
	
	@Override
	protected boolean containsOpen(IASTNode node) {
		FunctionNameParser parser = new FunctionNameParser(node, OPEN, new String[] { "const char *", "int" });
		return parser.matches();
	}

	@Override
	protected boolean containsClose(IASTNode node) {
		FunctionNameParser parser = new FunctionNameParser(node, CLOSE, new String[] { "int" });
		return parser.matches();
	}
	
	protected void reportProblem(IASTNode node, ExecutionState condition) {
		String message;
		if (condition.isTop()) {
			message = "Improper use of open/close.";	
		}
		else {
			message = MessageFormat.format("Improper use of open/close given {0}.", condition);			
		}
		reportProblem(ERR_ID, node, message);
	}

}
