/*******************************************************************************
 * Copyright (c) 2009 Elliott Baron
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Elliott Baron - initial API and implementation
 *******************************************************************************/
package org.eclipse.cdt.codan.extension.checkers;

import java.text.MessageFormat;

import org.eclipse.cdt.codan.extension.Activator;
import org.eclipse.cdt.codan.extension.ExecutionState;
import org.eclipse.cdt.core.dom.ast.IASTNode;

public class FOpenFCloseChecker extends AbstractOpenCloseChecker {
	private static final String ERR_ID = Activator.PLUGIN_ID + ".FOpenFCloseFilesProblem";
	
	private static final String FOPEN = "fopen";
	private static final String FCLOSE = "fclose";
	
	@Override
	protected boolean containsOpen(IASTNode node) {
		FunctionNameParser parser = new FunctionNameParser(node, FOPEN, new String[] { "const char * restrict", "const char * restrict" });
		return parser.matches();
	}

	@Override
	protected boolean containsClose(IASTNode node) {
		FunctionNameParser parser = new FunctionNameParser(node, FCLOSE, new String[] { "FILE *" });
		return parser.matches();
	}
	
	protected void reportProblem(IASTNode node, ExecutionState condition) {
		String message;
		if (condition.isTop()) {
			message = "Improper use of fopen/fclose.";	
		}
		else {
			message = MessageFormat.format("Improper use of fopen/fclose given {0}.", condition);			
		}
		reportProblem(ERR_ID, node, message);
	}

}
