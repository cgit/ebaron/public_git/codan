/*******************************************************************************
 * Copyright (c) 2009 Elliott Baron
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Elliott Baron - initial API and implementation
 *******************************************************************************/
package org.eclipse.cdt.codan.extension;

import org.eclipse.cdt.core.dom.ast.IVariable;

public class ExecutionStateClause {
	
	protected IVariable var;
	protected boolean truthValue;

	public ExecutionStateClause(IVariable var) {
		this(var, true);
	}
	
	public ExecutionStateClause(IVariable var, boolean truthValue) {
		this.var = var;
		this.truthValue = truthValue;
	}

	public IVariable getVariable() {
		return var;
	}
	
	public boolean isTrue() {
		return truthValue;
	}
	
	public ExecutionStateClause getNegation() {
		return new ExecutionStateClause(var, !truthValue);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof ExecutionStateClause) {
			ExecutionStateClause other = (ExecutionStateClause) obj;
			return var.equals(other.getVariable()) && truthValue == other.isTrue();
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return var.hashCode() + (truthValue ? 1 : 0);
	}
	
	@Override
	public String toString() {
		StringBuffer buf = new StringBuffer();
		if (!truthValue) {
			buf.append("NOT ");
		}
		buf.append(var.getName());
		return buf.toString();
	}
	
}
