/*******************************************************************************
 * Copyright (c) 2009 Elliott Baron
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Elliott Baron - initial API and implementation
 *******************************************************************************/
package org.eclipse.cdt.codan.extension;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.cdt.codan.extension.Minterm.Value;
import org.eclipse.cdt.core.dom.ast.IVariable;

public class ESTruthTable implements ITruthTable<IVariable> {
	private List<Minterm<IVariable>> minterms;
	private Map<Minterm<IVariable>, ExecutionState> esMapping;
	private List<IVariable> variables;
	
	public ESTruthTable(Set<ExecutionState> es) {
		minterms = new ArrayList<Minterm<IVariable>>();
		variables = new ArrayList<IVariable>();
		esMapping = new HashMap<Minterm<IVariable>, ExecutionState>();
		
		for (ExecutionState e : es) {
			for (ExecutionStateClause c : e.getClauses()) {
				IVariable var = c.getVariable();
				if (!variables.contains(var)) {
					variables.add(var);
				}
			}
		}
		
		// Initialize with Don't Cares
		for (int i = 0; i < es.size(); i++) {
			Map<IVariable, Value> values = new HashMap<IVariable, Value>();
			for (IVariable var : variables) {
				values.put(var, Value.DONTCARE);
			}
			minterms.add(new Minterm<IVariable>(values));
		}
		
		// Set actual truth values
		Iterator<ExecutionState> it = es.iterator();
		for (int i = 0; i < es.size(); i++) {
			Minterm<IVariable> term = minterms.get(i);
			ExecutionState e = it.next();
			for (ExecutionStateClause c : e.getClauses()) {
				term.setValue(c.getVariable(), c.isTrue() ? Value.TRUE : Value.FALSE);
			}
			esMapping.put(term, e);
		}		
	}
	
	public List<Minterm<IVariable>> getMinterms() {
		return minterms;
	}
	
	public List<IVariable> getVariables() {
		return variables;
	}
	
	public ExecutionState getExecutionState(Minterm<IVariable> minterm) {
		return esMapping.get(minterm);
	}
	
	@Override
	public String toString() {
		StringBuffer buf = new StringBuffer();
		for (IVariable var : variables) {
			buf.append(var.getName());
			buf.append(" ");
		}
		buf.append("\n");
		for (Minterm<IVariable> term : minterms) {
			for (IVariable var : variables) {
				buf.append(term.getValue(var));
				buf.append(" ");
			}
			buf.append("\n");
		}
		return buf.toString();
	}

}
