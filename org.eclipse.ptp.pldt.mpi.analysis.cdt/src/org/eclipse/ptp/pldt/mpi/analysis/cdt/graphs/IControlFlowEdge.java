/**********************************************************************
 * Copyright (c) 2009 Elliott Baron.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Elliott Baron - initial API and implementation
 *******************************************************************************/
package org.eclipse.ptp.pldt.mpi.analysis.cdt.graphs;

/**
 * An edge in the {@link IControlFlowGraph} between two {@link IBlock} objects.
 */
public interface IControlFlowEdge {

	/**
	 * @return the block for which this is an outgoing edge
	 */
	public abstract IBlock getFrom();

	/**
	 * @return the block for which this is an incoming edge
	 */
	public abstract IBlock getTo();

}