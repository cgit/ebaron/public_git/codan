/**********************************************************************
 * Copyright (c) 2009 Elliott Baron.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Elliott Baron - initial API and implementation
 *******************************************************************************/
package org.eclipse.ptp.pldt.mpi.analysis.cdt.graphs.impl;

import org.eclipse.ptp.pldt.mpi.analysis.cdt.graphs.IBlock;
import org.eclipse.ptp.pldt.mpi.analysis.cdt.graphs.IControlFlowEdge;

public class ControlFlowEdge implements IControlFlowEdge {
	private IBlock from;
	private IBlock to;
	
	public ControlFlowEdge(IBlock from, IBlock to) {
		this.from = from;
		this.to = to;
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.ptp.pldt.mpi.analysis.cdt.graphs.IControlFlowEdge#getFrom()
	 */
	public IBlock getFrom() {
		return from;
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.ptp.pldt.mpi.analysis.cdt.graphs.IControlFlowEdge#getTo()
	 */
	public IBlock getTo() {
		return to;
	}

}
